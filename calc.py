#!/usr/bin/python3

def sumar(n1:float, n2:float) -> float:
    resultado = n1+n2
    print(n1, '+', n2, '=', resultado)

def restar(n1:float, n2:float) -> float:
    resultado = n1-n2
    print(n1, '-', n2, '=', resultado)

sumar(1,2)
sumar(3,4)
restar(6,5)
restar(8,7)
